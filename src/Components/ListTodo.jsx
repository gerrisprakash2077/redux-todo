import React, { Component } from 'react'
import { connect } from 'react-redux'
import { removeTodo, markCompleted, search } from '../../Redux'
class ListTodo extends Component {
    constructor(props) {
        super(props)
    }
    deleteHandler = (id) => {
        this.props.removeTodo(id)
    }
    markHandler = (id) => {
        // this.props.
        this.props.markCompleted(id)
    }
    handleSearch = (event) => {
        this.props.search(event.target.value)
    }
    render() {
        // console.log(this.props.data);

        return (
            <div>
                <input type='text'
                    placeholder="search task"
                    className="w-96 border-solid border-2 border-black rounded-lg mb-10 px-5 py-2"
                    onChange={this.handleSearch}
                />
                <div className='ml-auto mr-auto w-96 flex flex-col items-start '>
                    {this.props.data.list.map((task) => {
                        return <div key={task.id} className={task.searchFilter ? 'w-full flex justify-between items-center m-2' : 'hidden'}>
                            <input type="checkbox" className='mr-10 h-5 w-5 cursor-pointer' onClick={() => this.markHandler(task.id)}></input>
                            <p className={task.state == 'active' ? 'inline mr-10 ' : 'inline mr-10 line-through'}
                            >{task.task}</p>
                            <button className=' text-red-700' onClick={() => this.deleteHandler(task.id)}>X</button>
                        </div>
                    })}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        data: state
    }
}

const mapDispatchToProps = {
    removeTodo: removeTodo,
    markCompleted: markCompleted,
    search: search
}
export default connect(mapStateToProps, mapDispatchToProps)(ListTodo)
