import React, { Component } from 'react'
import { connect } from 'react-redux'
import { inputChange, addTodo } from '../Redux'
import ListTodo from './Components/ListTodo'
class App extends Component {
  constructor(props) {
    super(props)
  }
  changeHandler = (event) => {
    // console.log(event.target.value);
    this.props.inputChange(event.target.value)
  }
  addTodoHandler = (event) => {
    if (event.key == 'Enter' && event.target.value != '') {
      console.log("enter");
      this.props.inputChange('')
      this.props.addTodo(event.target.value)
    }
  }
  render() {
    return (
      <>
        <h1 className='text-center text-8xl text-red-200 font-black'>TODO</h1>
        <div className='text-center m-10 text-3xl'>
          <ListTodo />
          <input
            type='text'
            placeholder="What need to be done?"
            className="w-96 border-solid border-2 border-black rounded-lg mt-10 px-5 py-2"
            onChange={this.changeHandler}
            value={this.props.data.input}
            onKeyUp={this.addTodoHandler}
          />

        </div>
      </>

    )
  }
}
function mapStateToProps(state) {
  return {
    data: state
  }
}

const mapDispatchToProps = {
  inputChange: inputChange,
  addTodo: addTodo

}
export default connect(mapStateToProps, mapDispatchToProps)(App)
