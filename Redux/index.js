import { createStore } from "redux"
let ids = 0
if (!localStorage.getItem('todoData')) {
    localStorage.setItem('todoData', JSON.stringify({
        input: "",
        list: []
    }))
} else {
    let clown = JSON.parse(localStorage.getItem('todoData'))
    if (clown.list.length != 0) {
        ids = clown.list[clown.list.length - 1].id
    }
}

export function inputChange(value) {
    return {
        type: "INPUT_CHANGE",
        payload: value
    }
}
export function search(value) {
    return {
        type: 'SEARCH',
        payload: value
    }
}
export function addTodo(task) {
    return {
        type: "ADD_TODO",
        payload: {
            id: ++ids,
            task: task,
            state: 'active',
            searchFilter: true
        }
    }
}

export function removeTodo(id) {
    return {
        type: "REMOVE_TODO",
        payload: id
    }
}
export function markCompleted(id) {
    return {
        type: "MARK_COMPLETED",
        payload: id
    }
}
const initialState = JSON.parse(localStorage.getItem('todoData'))
function reducer(data = initialState, action) {
    switch (action.type) {
        case "INPUT_CHANGE":
            return {
                ...data,
                input: action.payload
            }
        case 'SEARCH':
            let searchList = data.list.map((elem) => {
                if (!elem.task.toLowerCase().includes(action.payload.toLowerCase())) {
                    elem.searchFilter = false
                } else {
                    elem.searchFilter = true
                }
                return elem
            })
            return {
                ...data,
                list: searchList
            }

        case 'ADD_TODO':
            return {
                ...data,
                list: [
                    ...data.list,
                    action.payload
                ]
            }
        case 'REMOVE_TODO':
            const removedList = data.list.filter((elem) => {
                return elem.id != action.payload
            })
            return {
                ...data,
                list: removedList
            }
        case 'MARK_COMPLETED':
            const markList = data.list.map((elem) => {
                if (elem.id == action.payload) {
                    if (elem.state == 'active') {
                        elem.state = 'completed'
                    } else {
                        elem.state = 'active'
                    }
                }
                return elem
            })
            return {
                ...data,
                list: markList
            }
        default:
            return data
    }
}

const store = createStore(reducer)
store.subscribe(() => {
    localStorage.setItem('todoData', JSON.stringify(store.getState()))
    console.log(store.getState())
})
export default store

